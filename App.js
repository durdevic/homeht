import { createStackNavigator, createSwitchNavigator } from "react-navigation"; // Version can be specified in package.json

import HomeScreen from "./src/screens/HomeScreen";
import SecondScreen from "./src/screens/SecondScreen";
import LoginScreen from "./src/screens/AuthScreens/LoginScreen";
import AuthLoadingScreen from "./src/screens/AuthScreens/AuthLoadingScreen";
import RegisterScreen from "./src/screens/AuthScreens/RegisterScreen";

console.disableYellowBox = true;

const AppStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: ({ navigation }) => ({
        headerBackTitle: null
      })
    },
    Other: {
      screen: SecondScreen,
      navigationOptions: ({ navigation }) => ({
        headerBackTitle: null
      })
    }
  },
  {
    navigationOptions: ({ navigation }) => ({
      header: null
    })
  }
);
const AuthStack = createStackNavigator({
  Login: LoginScreen,
  Register: {
    screen: RegisterScreen,
    navigationOptions: ({ navigation }) => ({
      headerBackTitle: null
    })
  }
},
{
  navigationOptions: ({ navigation }) => ({
    header: null,
    tabBarOptions: {
      activeTintColor: "red",
      inactiveTintColor: "gray"
    }
  })
}


);

export default createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack
  },
  {
    initialRouteName: "AuthLoading"
  }
);
