import React, { Component } from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";

class RadioButton extends Component {
  constructor(props) {
    super(props);
  }

  handleClick = () => {
    this.props.handler(this.props.index);
  };

  render() {
    return (
      <TouchableOpacity
        onPress={this.handleClick}
        style={this.props.isChecked ? styles.checked : styles.unchecked}
      >
        {/* <View className={this.props.isChecked ? "radiobtn checked" : "radiobtn unchecked"} data-value={this.props.value}></View> */}
        <Text>{this.props.text}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  // radio
  checked: {
    backgroundColor: "red"
  },
  unchecked: {}
});

export default RadioButton;
