import React, { Component } from 'react';
import { View } from 'react-native';
import RadioButton from './RadioButton';

class RadioGroup extends Component {
    constructor() {
      super();
      this.state = {
        selectedIndex: null,
        selectedValue: null,
        options: ["option 0", "option 1", "option 2", "option 3"]
      };
    }
  
    toggleRadioBtn = index => {
      this.setState({
        selectedIndex: index,
        selectedValue: this.state.options[index],
        options: this.state.options
      });
      console.log(index);
    };
  
    render() {
      const { options } = this.state;
  
      const allOptions = options.map((option, i) => {
        return (
          <RadioButton
            key={i}
            isChecked={this.state.selectedIndex == i}
            text={option}
            value={option}
            index={i}
            handler={this.toggleRadioBtn}
          />
        );
      });
  
      return (
        <View style={{ backgroundColor: "gray", borderRadius: 6 }}>
          {allOptions}
        </View>
      );
    }
  }

  export default RadioGroup;