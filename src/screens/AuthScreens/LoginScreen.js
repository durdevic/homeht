import React, { Component } from "react";
import { View, Text, TouchableOpacity, AsyncStorage, StyleSheet, StatusBar } from "react-native";

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }


  _signInAsync = async () => {
    await AsyncStorage.setItem("userToken", "abc");
    this.props.navigation.navigate("App");
  };

  _signUp = () => {
    this.props.navigation.navigate('Register')
  }

  render() {
    return (
      <View style={styles.container}>
        {/* <Button title="Sign in!" onPress={this._signInAsync} /> */}
        <StatusBar barStyle='light-content'></StatusBar>
        <Text style={{fontSize: 24, color: 'white', marginBottom: 50}}>Register for HOME</Text>
        <TouchableOpacity onPress={this._signUp}>
          <Text style={{color: 'white', fontSize: 20}}>Let's get started!</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: '#000000'
  },
  container2: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "red"
  }
});

export default LoginScreen;
