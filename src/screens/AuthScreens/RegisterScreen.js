import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  KeyboardAvoidingView,
  StatusBar
} from "react-native";
import Swiper from "react-native-swiper";
// import RadioGroup from "../../components/RadioGroup";
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel
} from "react-native-simple-radio-button";

class RegisterScreen extends Component {
  radio_props = [
    { label: "0 - 1.000", value: 0 },
    { label: "1.000 - 2.000", value: 1 },
    { label: "2.000 - 3.000", value: 2 },
    { label: "3.000 - 4.000", value: 3 },
    { label: "Mehr als 4.000", value: 4 }
  ];

  constructor(props) {
    super(props);
    this.state = {
      fullName: "",
      email: "",
      phoneNumber: "",
      sal: 0,
      emailValid: true
    };
  }

  validateEmail = email => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(email)) {
      return true;
    } else {
      return false;
    }
  };

  goToNextScreen = (type, data) => {
    switch (type) {
      case "name": {
        this.setState({ fullName: data });
        this.refs.swiper.scrollBy(1);
        break;
      }
      case "email": {
        console.log(data);

        if (this.validateEmail(data)) {
          this.setState({ emailValid: true });
          this.setState({ email: data });
          this.refs.swiper.scrollBy(1);
          break;
        } else {
          this.setState({ emailValid: false });
          break;
        }
      }
      case "phone": {
        this.setState({ phoneNumber: data });
        this.refs.swiper.scrollBy(1);
        break;
      }
      case "salary": {
        console.log(data);
        // this.setState({ sal: data });
        this.refs.swiper.scrollBy(1);
        break;
      }
      default: {
        return;
      }
    }
  };

  checkIfValid = emailText => {
    if (this.validateEmail(emailText)) {
      this.setState({ emailValid: true });
    } else {
      this.setState({ emailValid: false });
    }
  };

  finishUp = async () => {
    await AsyncStorage.setItem("userToken", "abc");
    this.props.navigation.navigate("App");
  };

  render() {
    let errorMsg;
    if (this.state.emailValid) {
      errorMsg = <Text />;
    } else {
      errorMsg = <Text style={{ color: "white" }}>Email invalid</Text>;
    }
    return (
      <KeyboardAvoidingView style={{ flex: 1 }}>
        <StatusBar barStyle="light-content" />
        <Swiper
          ref="swiper"
          style={styles.wrapper}
          activeDotColor={"white"}
          scrollEnabled={false}
          loop={false}
        >
          {/* FIRST SCREEN */}
          <KeyboardAvoidingView style={[styles.slideWrapper, styles.slide1]}>
            <Text style={styles.text}>How do we call you?</Text>
            <View style={{ flex: 1, justifyContent: "center" }}>
              <TextInput
                ref="textInputName"
                placeholderTextColor={"rgba(255,255,255,.7)"}
                autoCorrect={false}
                placeholder="Your name"
                style={{ fontSize: 24, color: "white" }}
              />
            </View>
            <TouchableOpacity
              style={styles.nextButton}
              onPress={() =>
                this.goToNextScreen(
                  "name",
                  this.refs.textInputName._lastNativeText
                )
              }
            >
              <Text style={styles.nextButtonText}>Next</Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>

          {/* SECOND SCREEN */}
          <KeyboardAvoidingView style={[styles.slideWrapper, styles.slide2]}>
            <Text style={styles.text}>Mail</Text>
            <View style={{ flex: 1, justifyContent: "center" }}>
              <TextInput
                ref="textInputEmail"
                placeholder="Your email address"
                placeholderTextColor={"rgba(255,255,255,.7)"}
                value={this.state.email}
                autoCapitalize="none"
                textContentType="emailAddress"
                style={{ fontSize: 24, color: "white" }}
                onBlur={() =>
                  this.checkIfValid(this.refs.textInputEmail._lastNativeText)
                }
              />
              {errorMsg}
            </View>
            <TouchableOpacity
              style={styles.nextButton}
              onPress={() =>
                this.goToNextScreen(
                  "email",
                  this.refs.textInputEmail._lastNativeText
                )
              }
            >
              <Text style={styles.nextButtonText}>Next</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{ color: "white" }}
              onPress={() => this.refs.swiper.scrollBy(-1)}
            >
              <Text style={styles.backButton}>Back</Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>

          {/* THIRD SCREEN */}
          <KeyboardAvoidingView style={[styles.slideWrapper, styles.slide3]}>
            <Text style={styles.text}>Phone Number</Text>
            <View style={{ flex: 1, justifyContent: "center" }}>
              <TextInput
                ref="textInputPhone"
                placeholder="Your phone number"
                placeholderTextColor={"rgba(255,255,255,.7)"}
                style={{ fontSize: 24, color: "white" }}
              />
            </View>
            <TouchableOpacity
              style={styles.nextButton}
              onPress={() =>
                this.goToNextScreen(
                  "phone",
                  this.refs.textInputPhone._lastNativeText
                )
              }
            >
              <Text style={styles.nextButtonText}>Next</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ color: "white" }}
              onPress={() => this.refs.swiper.scrollBy(-1)}
            >
              <Text style={styles.backButton}>Back</Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>

          {/* FOURTH SCREEN */}
          <KeyboardAvoidingView style={[styles.slideWrapper, styles.slide4]}>
            <Text style={styles.text}>Your salary is?</Text>
            <View style={{ flex: 1, justifyContent: "center" }}>
              <RadioForm
                radio_props={this.radio_props}
                initial={0}
                buttonColor={"white"}
                labelColor={"white"}
                selectedButtonColor={"white"}
                selectedLabelColor={"white"}
                labelC
                onPress={value => {
                  console.log(value);
                  this.setState({ sal: value });
                  console.log(this.state);
                }}
              />
            </View>
            <TouchableOpacity
              style={styles.nextButton}
              onPress={() =>
                this.goToNextScreen("salary", this.refs.radioGroup)
              }
            >
              <Text style={styles.nextButtonText}>Next</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ color: "white" }}
              onPress={() => this.refs.swiper.scrollBy(-1)}
            >
              <Text style={styles.backButton}>Back</Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>

          {/* FINAL SCREEN */}
          <KeyboardAvoidingView style={[styles.slideWrapper, styles.slide5]}>
            <Text style={styles.text}>Everything correct?</Text>
            <View style={{ flex: 1, minWidth: "70%", paddingVertical: 20 }}>
              <Text style={{ color: "white" }}>Full Name</Text>
              <Text style={styles.summaryText}>{this.state.fullName}</Text>
              <Text style={{ color: "white" }}>Email</Text>
              <Text style={styles.summaryText}>{this.state.email}</Text>
              <Text style={{ color: "white" }}>Phone number</Text>
              <Text style={styles.summaryText}>{this.state.phoneNumber}</Text>
              <Text style={{ color: "white" }}>Salary</Text>
              <Text style={styles.summaryText}>
                {this.radio_props[this.state.sal].label}
              </Text>
            </View>
            <TouchableOpacity
              style={styles.doneButton}
              onPress={() => this.finishUp()}
            >
              <Text>Done</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ color: "white" }}
              onPress={() => this.refs.swiper.scrollBy(-1)}
            >
              <Text style={styles.backButton}>Back</Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </Swiper>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  slideWrapper: {
    flex: 1,
    paddingVertical: 100,
    justifyContent: "center",
    alignItems: "center"
  },
  slide1: {
    backgroundColor: "#9DD6EB"
  },
  slide2: {
    backgroundColor: "#97CAE5"
  },
  slide3: {
    backgroundColor: "#92BBD9"
  },
  slide4: {
    backgroundColor: "#9A8BD9"
  },
  slide5: {
    backgroundColor: "#9D1BD9"
  },
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold"
  },
  nextButton: {
    borderRadius: 50,
    paddingVertical: 7,
    paddingHorizontal: 16,
    borderColor: "white",
    borderWidth: 1
  },
  nextButtonText: {
    fontSize: 16,
    color: "white"
  },
  backButton: {
    fontSize: 16,
    color: "white",
    marginTop: 14
  },
  doneButton: {
    backgroundColor: "white",
    borderRadius: 50,
    paddingVertical: 10,
    paddingHorizontal: 20
  },
  summaryText: {
    color: "white",
    fontSize: 18,
    marginTop: 3,
    marginBottom: 10,
    fontWeight: "600"
  },

  // radio
  checked: {
    backgroundColor: "red"
  },
  unchecked: {}
});

export default RegisterScreen;
