import React from "react";
import {
  ActivityIndicator,
  AsyncStorage,
  Button,
  StatusBar,
  StyleSheet,
  View,
  Image,
  Text,
  ImageBackground,
  TouchableOpacity
} from "react-native";

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: "Welcome to the app!"
  };

  render() {
    return (
      <ImageBackground
        source={require("../assets/background.jpg")}
        style={{ width: "100%", flex: 1, resizeMode: "cover" }}
      >
        <StatusBar barStyle="light-content" />
        <View style={styles.container}>
          <Text style={{ fontSize: 30, color: "white", marginBottom: 100 }}>
            Welcome to our HOME! :)
          </Text>
          <TouchableOpacity onPress={this._signOutAsync}>
            <Text style={{ fontSize: 26, color: "white" }}>Sign me out</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }

  _showMoreApp = () => {
    this.props.navigation.navigate("Other");
  };

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate("Auth");
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,.2)"
  },
  container2: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "red"
  }
});

export default HomeScreen;
